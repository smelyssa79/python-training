#exercicio 1
# def saudacao(nome):
#     print(f"Olá {nome}! Tudo bem com você?")
#     return

# nome = input("Qual o seu nome? ")
# saudacao(nome)

#exercicio 2
# soma = lambda x, y: x + y
# sub = lambda x, y: x - y 
# mult = lambda x, y: x * y
# div = lambda x, y: x / y

# num1 = int(input("Digite o primeiro número: "))
# num2 = int(input("Digite o segundo número: "))

# print(f"""
# 1 - Soma
# 2 - Subtração
# 3 - Multiplicação
# 4 - Divisão 
# """)

# opc = int(input("Escolha uma das opções de cálculo: "))

# if opc == 1:
#     print(soma(num1, num2))
# elif opc == 2:
#     print(sub(num1, num2))
# elif opc == 3:
#     print(mult(num1, num2))
# elif opc == 4:
#     print(div(num1, num2))
# else:
#     print("Opção inválida!")

#exercicio 3
# def ver_cesta(cesta, total_gasto):
#     if not cesta:
#         print("Cesta vazia!") 
#     else:
#         print("--- Cesta de compras ---")
#         for fruta in cesta:
#             print(fruta)
            
#         print(f"Total a pagar: R${total_gasto}")
#     print("")

# def adicionar_fruta(cesta, total_gasto):
#     while True:
#         print(f"""
#         1 - Banana
#         2 - Melancia
#         3 - Morango
#         """)

#         fruta_escolhida = input("Digite a opção desejada: ")

#         if fruta_escolhida in ['1', '2', '3']:
#             frutas = ['Banana', 'Melancia', 'Morango']
#             precos_frutas = [2.50, 5.0, 3.50]

#             cesta.append(frutas[int(fruta_escolhida) - 1])

#             preco = precos_frutas[int(fruta_escolhida) - 1]
#             total_gasto += preco

#             print(f"{frutas[int(fruta_escolhida) - 1]} adicionada com sucesso!")
#             break
#         else:
#             print("Opção inválida. Digite novamente.")

#     return (cesta, total_gasto)

# cesta = []
# total_gasto = 0

# while True:
#     print(f"""Menu principal: 
#     Quitanda:
#     1: Ver cesta
#     2: Adicionar frutas
#     3: sair
#     """)

#     resposta = input("Digite a opção desejada: ")

#     if resposta == '1':
#         ver_cesta(cesta, total_gasto)

#     elif resposta == '2':
#         cesta, total_gasto = adicionar_fruta(cesta, total_gasto)

#     elif resposta == '3':
#         print("--- Compra finalizada ---")
#         for fruta in cesta:
#             print(fruta)
            
#         print(f"Total a pagar: R${total_gasto}")
#         break

#     else:
#         print("Opção inválida. Digite novamente.")

#exercicio 4
# def num_pares(list_num):
#     qtd_pares = 0
#     list_pares = []

#     for num in list_num :
#         if(num % 2 == 0):
#             qtd_pares = qtd_pares + 1
#             list_pares.append(num)
        
#     print(f"Você me passou {qtd_pares} números pares, sendo eles: {list_pares}.")

# count = 0
# num_user = []

# numeros = print("Entre com 10 números inteiros para descobrir quais deles são pares: ")

# while count < 10:
#     num_user.append(int(input()))
#     count = count+1

# num_pares(num_user)

#exercicio 5
calcula_area = lambda x, y: x * y

def calcula_latas(area):
    qtd_latas = area / 3
    return(qtd_latas)


altura = float(input("Entre com a altura da parede: "))
largura = float(input("Entre com a largura da parede: "))

area = calcula_area(altura, largura)
qtd_latas = calcula_latas(area)

print(f"A área de sua parede é de {area:.2f}m², portanto seriam necessárias {qtd_latas:.2f} latas de tinta.")




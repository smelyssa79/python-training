#exercicio 1
# frase = "Umxpratoxdextrigoxparaxtrêsxtigresxtristes"
# print(frase.replace("x", " "))


#exercicio 2
# ano = int(input("Qual o ano do seu nascimento? "))

# if ano <= 1964:
#     print("Geração: Baby Boomer")

# elif ano >= 1965 and ano <= 1979:
#     print("Geração: X")

# elif ano >= 1980 and ano <= 1994:
#     print("Geração: Y")

# else:
#     print("Geração: Z")


#exercicio 3
# cesta = []

# while True:
#     print(f"""Menu principal: 
#     Quitanda:
#     1: Ver cesta
#     2: Adicionar frutas
#     3: sair
#     """)

#     resposta = input("Digite a opção desejada: ")

#     if resposta == '1':
#         if not cesta:
#             print("Cesta vazia!") 
#         else:
#             print("--- Cesta de compras ---")
#             for fruta in cesta:
#                 print(fruta)
            

#     elif resposta == '2':
#         while True:
#             print(f"""
#             1 - Banana
#             2 - Melancia
#             3 - Morango
#             """)

#             fruta_escolhida = input("Digite a opção desejada: ")

#             if fruta_escolhida in ['1', '2', '3']:
#                 frutas = ['Banana', 'Melancia', 'Morango']

#                 cesta.append(frutas[int(fruta_escolhida) - 1])
#                 print(f"{frutas[int(fruta_escolhida) - 1]} adicionada com sucesso!")
#                 break
#             else:
#                 print("Opção inválida. Digite novamente.")

#     elif resposta == '3':
#         break

#     else:
#         print("Opção inválida. Digite novamente.")


#exercicio 4

cesta = []
total_gasto = 0

while True:
    print(f"""Menu principal: 
    Quitanda:
    1: Ver cesta
    2: Adicionar frutas
    3: sair
    """)

    resposta = input("Digite a opção desejada: ")

    if resposta == '1':
        if not cesta:
            print("Cesta vazia!") 
        else:
            print("--- Cesta de compras ---")
            for fruta in cesta:
                print(fruta)
            
            print(f"Total a pagar: R${total_gasto}")

    elif resposta == '2':
        while True:
            print(f"""
            1 - Banana
            2 - Melancia
            3 - Morango
            """)

            fruta_escolhida = input("Digite a opção desejada: ")

            if fruta_escolhida in ['1', '2', '3']:
                frutas = ['Banana', 'Melancia', 'Morango']
                precos_frutas = [2.50, 5.0, 3.50]

                cesta.append(frutas[int(fruta_escolhida) - 1])
                print(f"{frutas[int(fruta_escolhida) - 1]} adicionada com sucesso!")

                preco = precos_frutas[int(fruta_escolhida) - 1]
                total_gasto += preco
                break
            else:
                print("Opção inválida. Digite novamente.")

    elif resposta == '3':
        print("--- Compra finalizada ---")
        for fruta in cesta:
            print(fruta)
            
        print(f"Total a pagar: R${total_gasto}")
        break

    else:
        print("Opção inválida. Digite novamente.")
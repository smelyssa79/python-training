# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# placa
# modelo
# movimento

# Os comportamentos esperados para um Ônibus são:

# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

# class Onibus:

#     def __init__(self):
#         self.__capacidade_total = 45
#         self.__passageiros = 0
#         self.__movimento = False
#         self.__placa = "HKT-5239"
#         self.__modelo = "Escolar"

#     def embarcar(self, novos_passageiros):
#         if self.__movimento == True:
#             return("O ônibus está em movimento")
#         elif self.__passageiros == self.__capacidade_total:
#             return("O ônibus está lotado!")
#         else:
#             if self.__passageiros + novos_passageiros > self.__capacidade_total:
#                 total_passageiros = self.__passageiros + novos_passageiros
#                 passageiros_de_fora = total_passageiros - self.__capacidade_total
#                 conseguiram_entrar = novos_passageiros - passageiros_de_fora

#                 self.__passageiros = 45
#                 return(f"Apenas {conseguiram_entrar} pessoas conseguiram entrar, {passageiros_de_fora} ficaram de fora porque o ônibus lotou")

#             else:
#                 self.__passageiros += novos_passageiros
#                 return(f"Todos os {novos_passageiros} passageiros entraram no ônibus")

#     def desembarcar(self, passageiros_saindo):
#         if self.__movimento == True:
#             return("O ônibus está em movimento")
#         elif self.__passageiros == 0:
#             return("Não há ninguém para desembarcar")
#         elif self.__passageiros < passageiros_saindo:
#             return(f"Não há {passageiros_saindo} pessoas para desemarcar, há apenas {self.__passageiros} pessoas nesse momento")
#         else:
#             self.__passageiros -= passageiros_saindo
#             return(f"{passageiros_saindo} passageros desembarcaram do ônibus, sobrando {self.__passageiros} pessoas no ônibus")

#     def acelerar(self):
#         if self.__movimento == True:
#             return("O ônibus já está em movimento")
#         else:
#             self.__movimento = True
#             return("O ônibus começou a se mover")

#     def frear(self):
#         if self.__movimento == False:
#             return("O ônibus já está parado")
#         else:
#             self.__movimento = False
#             return("O ônibus parou")


# onibus_escolar = Onibus()

# print(onibus_escolar.acelerar())
# print(onibus_escolar.frear())
# print(onibus_escolar.embarcar(10))
# print(onibus_escolar.acelerar())
# print(onibus_escolar.frear())
# print(onibus_escolar.desembarcar(5))


# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class Fila:

    def __init__(self):
        self.pilha = []
        self.__topo = 0

    def adicionar(self, cliente):
        if self.pilha.count == 0:
            self.pilha.append(cliente)
            self.__topo += 1
            return(f"Cliente de {cliente} anos entrou na fila!")

        elif cliente > 65:
            self.pilha.insert(0, cliente)
            self.__topo += 1
            return(f"Cliente prioritário de {cliente} anos entrou na fila!")

        else:
            self.pilha.append(cliente)
            self.__topo += 1
            return(f"Cliente de {cliente} anos entrou na fila!")

    def atender_fila(self):
        if self.pilha.count == 0:
            return("Não há ninguém para atender!")
        else:
            cliente_atendido = self.pilha.pop(0)
            return(f"Cliente de {cliente_atendido} anos está sendo atendido")

    def checar_fila(self):
        return self.pilha
        

fila_banco = Fila()

print(fila_banco.adicionar(27))
print(fila_banco.checar_fila())
print(fila_banco.adicionar(31))
print(fila_banco.adicionar(20))
print(fila_banco.adicionar(66))
print(fila_banco.checar_fila())
print(fila_banco.atender_fila())
print(fila_banco.checar_fila())


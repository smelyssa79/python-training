#exercicio 1
nome = input("Insira o seu nome: ")
cpf = input ("Insira o seu cpf: ")
idade = input("Insira sua idade: ")

cpf = '{}.{}.{}-{}'.format(cpf[:3], cpf[3:6], cpf[6:9], cpf[9:])

print(f"Confirmação de cadastro: \nNome: {nome} \nCpf: {cpf} \nIdade: {idade}")

#exercicio 2
num_1 = int(input("Insira o primeiro número: "))
num_2 = int(input("Insira o segundo número: "))

soma = num_1 + num_2
dif = num_1 - num_2

print(f"Soma: {num_1} + {num_2} = {soma}")
print(f"Diferença: {num_1} - {num_2} = {dif}")